const fs = require('fs');
const { readFile } = require('fs/promises');
const superagent = require('superagent');

const readFilePromise = (file) => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, (err, data) => {
      if (err) reject('I could not find that file!');
      resolve(data);
    });
  });
};

//without promises
/*fs.readFile(`${__dirname}/dog.txt`, (err, data) => {
  console.log(`Breed: ${data}`);

  superagent
    .get(`https://dog.ceo/api/breed/${data}/images/random`)
    .end((err, result) => {
      if (err) return console.log(err.message);
      console.log(result.body.message);

      fs.writeFile('dog-img.txt', result.body.message, (err) => {
        if (err) return console.log(err.message)
        console.log('Random image of dog');
      });
    });
});*/

const writeFilePromise = (file, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, (err) => {
      if (err) reject('Could not write file!');
      resolve('success');
    });
  });
};

const getDogPic = async () => {
  try {
    const data = await readFilePromise(`${__dirname}/dog.txt`);
    console.log(`Breed: ${data}`);

    const response1Promise = superagent.get(
      `https://dog.ceo/api/breed/${data}/images/random`
    );

    const response2Promise = superagent.get(
      `https://dog.ceo/api/breed/${data}/images/random`
    );

    const response3Promise = superagent.get(
      `https://dog.ceo/api/breed/${data}/images/random`
    );
    const all = await Promise.all([
      response1Promise,
      response2Promise,
      response3Promise,
    ]);
    const imgs = all.map((element) => element.body.message);
    console.log(imgs);

    await writeFilePromise('dog-img.txt', imgs.join('\n'));
    console.log('Random dog image saved to file!');
  } catch (err) {
    console.log(err);
    throw err;
  }
  return '2: READY';
};

(async () => {
  try {
    console.log('1: Will get dog pics!');
    const x = await getDogPic();
    console.log(x);
    console.log('3: Done getting dog pics!');
  } catch (err) {
    console.log('ERROR');
  }
})();

/*
console.log('1: Will get dog pics!');
getDogPic()
  .then((x) => {
    console.log(x);
    console.log('3: Done getting dog pics!');
  })
  .catch((err) => {
    console.log('ERROR');
  });
*/

//Previous example code

/*
readFilePromise(`${__dirname}/dog.txt`)
  .then((result) => {
    console.log(`Breed: ${result}`);
    return superagent.get(`https://dog.ceo/api/breed/${result}/images/random`);
  })
  .then((response) => {
    console.log(response.body.message);

    return writeFilePromise('dog-img.txt', response.body.message);

    // fs.writeFile('dog-img.txt', response.body.message, (err) => {
    //   if (err) return console.log(err.message);
    //   console.log('Random image of dog');
    // });
  })
  .then(() => {
    console.log('Random dog image saved to file!');
  })
  .catch((err) => {
    console.log(err);
  });
*/
